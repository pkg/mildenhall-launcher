/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "launcher.h"
#include "mildenhall/mildenhall_ui_utility.h"

#include <canterbury/canterbury-platform.h>
#include <clutter/gdk/clutter-gdk.h>
#include <glib/gi18n.h>
#include <glib-unix.h>
#include <locale.h>

G_DEFINE_TYPE (MildenhallLauncher, mildenhall_launcher, CLUTTER_TYPE_ACTOR)

#define MILDENHALL_LAUNCHER_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), MILDENHALL_TYPE_LAUNCHER, MildenhallLauncherPrivate))


MildenhallLauncher *defaultLauncher = NULL;
ClutterActor   *stage = NULL;
ClutterActor *get_default_launcher()
{
  return CLUTTER_ACTOR(defaultLauncher);
}

static void mildenhall_launcher_finalize (GObject *object)
{
	/* Free all memory held */
	defaultLauncher = NULL;
}

static void
mildenhall_launcher_dispose (GObject *object)
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (object)->priv;

  g_clear_object (&priv->entry_point_index);

  G_OBJECT_CLASS (mildenhall_launcher_parent_class)->dispose (object);
}

static void mildenhall_launcher_class_init (MildenhallLauncherClass *klass)
{
	GObjectClass *o_class = (GObjectClass *) klass;

	/* Assign pointers to our functions */
	o_class->finalize = mildenhall_launcher_finalize;
	o_class->dispose = mildenhall_launcher_dispose;

	g_type_class_add_private (klass, sizeof (MildenhallLauncherPrivate));

}

static void mildenhall_launcher_init (MildenhallLauncher *self)
{
  g_autoptr (CbyComponentIndex) component_index = NULL;
  g_autoptr (GError) error = NULL;

  self->priv = MILDENHALL_LAUNCHER_PRIVATE (self);
  self->priv->bTooltipShown = FALSE ;
  self->priv->enLauncherState = LAUNCHER_INITIAL ;

  component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE,
                                             &error);
  if (component_index == NULL)
    {
      g_error ("Unable to initialize component index, aborting: %s", error->message);
    }

  self->priv->entry_point_index = cby_entry_point_index_new (component_index);
}

static gboolean
quit_main_loop (gpointer user_data)
{
  clutter_main_quit ();
  return G_SOURCE_REMOVE;
}

int main(int argc, char *argv[])
{
	ClutterColor    black = { 0, 0, 0, 255 };
	GdkWindow *gdk_window;

    cby_init_environment ();

	clutter_set_windowing_backend ("gdk");
	g_set_prgname (MILDENHALL_LAUNCHER_APP_NAME);

	setlocale (LC_ALL, "");

	//Configure path and the name of language/mo-files.
	bindtextdomain (GETTEXT_PACKAGE, DATADIR "/locale");
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	if (clutter_init(&argc, &argv) != CLUTTER_INIT_SUCCESS)
		return -1;

	g_unix_signal_add (SIGINT, quit_main_loop, NULL);
	g_unix_signal_add (SIGTERM, quit_main_loop, NULL);

	stage = clutter_stage_new();
	clutter_actor_set_background_color(stage, &black);
	clutter_actor_set_size (stage,866,480);
	clutter_stage_set_use_alpha(CLUTTER_STAGE(stage), TRUE);
	clutter_actor_show (stage);

	gdk_window = clutter_gdk_get_stage_window (CLUTTER_STAGE (stage));
	gdk_window_set_decorations (gdk_window, 0);

	clutter_actor_set_reactive(CLUTTER_ACTOR( stage) , FALSE );

	defaultLauncher = g_object_new (MILDENHALL_TYPE_LAUNCHER, NULL);
  clutter_actor_add_child (stage, CLUTTER_ACTOR (defaultLauncher));
  clutter_actor_set_reactive (CLUTTER_ACTOR (defaultLauncher), TRUE);

  MildenhallLauncherPrivate *priv = defaultLauncher->priv;
  ClutterActor   *bkg = mildenhall_ui_utils_create_application_background();

  clutter_actor_set_scale(bkg , 1.1,1.0);
  clutter_actor_add_child (CLUTTER_ACTOR (defaultLauncher), bkg);
  priv->stage = stage;
  /* Starts watching name on the bus specified by bus_type */
  /* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
  g_bus_watch_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Canterbury",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    app_mgr_name_appeared,
                    app_mgr_name_vanished,
                    defaultLauncher,
                    NULL);

  g_bus_watch_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Canterbury.Mutter.Plugin",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    mutter_name_appeared,
                    mutter_name_vanished,
                    defaultLauncher,
                    NULL);

  populate_launcher_from_canterbury_platform (defaultLauncher);

  clutter_main ();

  g_clear_object (&defaultLauncher);

  return 0;
}
