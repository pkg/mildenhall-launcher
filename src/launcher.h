/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_LAUNCHER_H__
#define __MILDENHALL_LAUNCHER_H__

#include "barkway-enums.h"
#include "barkway.h"
#include "canterbury.h"
#include "canterbury_app_handler.h"
#include "liblightwood-container.h"
#include <canterbury/canterbury-platform.h>
#include <clutter/clutter.h>
#include <glib-object.h>
#include <mildenhall/mildenhall.h>
#include <mx/mx.h>
#include <thornbury/thornbury.h>

/* *********************************************************************
                app name
 *********************************************************************** */
#define MILDENHALL_LAUNCHER_APP_NAME   	"Launcher"
/* *********************************************************************
                Macro definitions
 *********************************************************************** */

#define WIDTH                   	866
#define HEIGHT                  	480
#define BACKGROUND_FILE_NAME    	PKGDATADIR"/background.png"
#define MASK_FILE_NAME          	PKGDATADIR"/content_mask_alpha.png"
#define OVERLAY_FILE_NAME       	PKGDATADIR"/content_overlay.png"
#define SHADE_FILE_NAME       		PKGDATADIR"/rolls_normal_seamless.png"
#define SCROLL_BAR_WIDTH        	24
#define CONTENT_OVERLAY_FILE_NAME	PKGDATADIR"/content_overlay_full.png"
#define MERGE_MASK3_FILE_NAME		  PKGDATADIR"/MergeMask3.png"
#define MILDENHALL_STATUS_BAR_FILE_NAME	PKGDATADIR"/StatusBar_DRAGON_plain.png"
#define abc_LOGO_FILE_NAME	    PKGDATADIR"/mb_marquee_btn_abc.png"
#define MERGE_MASK4_FILE_NAME		  PKGDATADIR"/MergeMask4.png"
#define MERGE_MASK5_FILE_NAME		  PKGDATADIR"/MergeMask5.png"
#define BACKGROUND_MINI_FILE_NAME	PKGDATADIR"/background-mini.png"
#define BACK_BUTTON             	PKGDATADIR"/icon_back.png"
#define HOME_BUTTON	              PKGDATADIR"/icon_nowplaying_AC.png"
#define NOW_PLAYING_BUTTON      	PKGDATADIR"/icon_home.png"
#define SCROLL_BAR_WIDTH        	24
#define MAIN_MENU_ITEMS 		      10
#define COL 				              20
#define MILDENHALL_STATUS_BAR_TEXT	      "abc Multimedia Reference System"





G_BEGIN_DECLS

#define MILDENHALL_TYPE_LAUNCHER mildenhall_launcher_get_type()

#define MILDENHALL_LAUNCHER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  MILDENHALL_TYPE_LAUNCHER, MildenhallLauncher))

#define MILDENHALL_LAUNCHER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  MILDENHALL_TYPE_LAUNCHER, MildenhallLauncherClass))

#define MILDENHALL_IS_LAUNCHER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  MILDENHALL_TYPE_LAUNCHER))

#define MILDENHALL_IS_LAUNCHER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  MILDENHALL_TYPE_LAUNCHER))

#define MILDENHALL_LAUNCHER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  MILDENHALL_TYPE_LAUNCHER, MildenhallLauncherClass))


#define  LAUNCHER_FREE_MEM_IF_NOT_NULL(pointer) 		if(NULL != pointer) \
		{ \
	g_free(pointer); \
	pointer = NULL; \
		}

#define   LAUNCHER_CHECK_IMAGE_PATH(pImagePath , pReturnPath) if(g_str_has_prefix(pImagePath , "file://")) \
{\
	pReturnPath = g_strdup(pImagePath );  \
} \
else \
{ \
	pReturnPath = g_strconcat( PKGDATADIR ,"/",pImagePath, NULL) ;\
}



typedef struct _MildenhallLauncher        MildenhallLauncher;
typedef struct _MildenhallLauncherClass   MildenhallLauncherClass;
typedef struct _MildenhallLauncherPrivate MildenhallLauncherPrivate;


typedef enum _enLauncherStates LauncherStates;
enum _enLauncherStates
{
	LAUNCHER_INITIAL,
	LAUNCHER_SHOWN ,
	LAUNCHER_ANIMATION_IN_PROGRESS,
	LAUNCHER_MINIMIZED,
};

typedef enum _enLaunchStates enLaunchstates;
enum _enLaunchStates
{
	APP_LAUNCH_ENABLED,
	APP_LAUNCH_DISABLED
};
typedef struct _SearchData
{
	gchar *pSearchString;
	gchar *pCategory;
	gchar *pName;
	gchar *pIcon;
	gchar *uId;
	gchar *pCategoySearchId;
}SearchData;

struct _MildenhallLauncher
{
  /*< private >*/
  ClutterActor parent_instance;

  MildenhallLauncherPrivate *priv;
};

struct _MildenhallLauncherClass
{
  /*< private >*/
  ClutterActorClass parent_class;
};

GType         mildenhall_launcher_get_type         (void) G_GNUC_CONST;

G_END_DECLS

struct _MildenhallLauncherPrivate
{
	GList* quick_menu_static_local;
  guint launched_app_current_state;
	GList* main_menu_label;
  GList* main_menu_label_icon;
  GList* quick_menu_label;
  GList* quick_menu_label_icon;
  gboolean internet_status;
  gboolean initialization_status;
  CanterburyLauncher *launcher_proxy;
  CanterburyAppManager *app_mgr_proxy;
  CanterburyMutterPlugin *mutter_proxy;
  ClutterActor   *stage;
  ClutterActor *drawer;
  //ClutterActor   *hbox1;
  //ClutterActor   *hbox2;
 	ClutterActor   *master_roller;
	ClutterActor   *slave_roller;
	ThornburyModel   *quick_menu_model;
	ThornburyModel   *main_menu_model;
  GHashTable *MainToQuickmap;	//Hash table for Main to Quickmenu
  GHashTable *QuickToMain;	//Hash table for Quickmenu to Main menu.
  gboolean MainMenufocus;		//Flag on Mainmenu on focus.
  gboolean QuickMenufocus;	//Flag on quickmenu on focus.
  CanterburyHardKeys* app_mgr_hk_proxy;
  gboolean bTooltipShown ;

  CbyEntryPointIndex *entry_point_index;
  LauncherStates enLauncherState;
	/* Ramya: Adding speller and menu drawer */
	ThornburyModel *menu_drawer_model;
	ClutterActor *menu_drawer_button;
	ThornburyModel *speller_model;
	ClutterActor *speller;
	gchar *pToggleButton;
    gchar *pSpellerData;
	BarkwayService *popup_proxy;
	gchar *pSearchId;
	GList *searchList;
	GList *pSearchResultList;

	gboolean is_master_roller_scrolling;
	gboolean is_slave_roller_scrolling;
};

/* model columns */
enum
{
        DRAWER_COLUMN_NAME,
        DRAWER_COLUMN_ICON,
        DRAWER_COLUMN_TOOLTIP_TEXT,
        DRAWER_COLUMN_REACTIVE,
        DRAWER_COLUMN_LAST
};

ClutterActor *get_default_launcher();
void init_launcher_ui(ClutterActor *stage ,MxWindow *window);
void new_app_status( CanterburyAppManager  *object,const gchar *app_name,gint new_app_state, GVariant *arguments,gpointer user_data);
void launcher_register_clb( GObject *source_object,GAsyncResult *res, gpointer user_data);
void app_mgr_proxy_clb( GObject *source_object,GAsyncResult *res, gpointer user_data);
void app_mgr_name_appeared (GDBusConnection *connection,const gchar *name,const gchar *name_owner,gpointer user_data);
void app_mgr_name_vanished( GDBusConnection *connection,const gchar *name,gpointer user_data);
void launcher_proxy_clb( GObject *source_object,GAsyncResult *res,gpointer user_data);
void app_mgr_launcher_service_name_appeared ( GDBusConnection *connection,const gchar *name,const gchar *name_owner, gpointer user_data);
void app_mgr_launcher_service_name_vanished( GDBusConnection *connection, const gchar  *name, gpointer user_data);
void populate_rollers( MildenhallLauncher *launcher , gchar **main_menu_label, gchar **main_menu_icon, gchar **quick_menu_label, gchar **quick_menu_icon );
void insert_quick_menu_entry_into_model (MildenhallLauncher *launcher, 
                                         ThornburyModel *model, 
                                         const gchar *main_menu, 
                                         const gchar *quick_menu,
                                         const gchar *quick_menu_icon,
                                         const gchar *pStatus, 
                                         gdouble progress);
void append_new_menu_entry_into_model( MildenhallLauncher* launcher , const gchar* main_menu ,
                                       const gchar* main_menu_icon , const gchar* quick_menu ,const gchar* quick_menu_icon ,const gchar* pStatus , gdouble progress);
gboolean is_item_in_model( ThornburyModel* model , const gchar* item );
void remove_menu_entry_from_model( MildenhallLauncher* launcher , const gchar *p_quick_menu );
gchar* get_new_main_to_quick_map(MildenhallLauncher* launcher , const gchar *main_menu , guint row_cnt);
void remove_main_menu_model_item(MildenhallLauncher* launcher , const gchar *main_menu);
void on_notify_current_active_menu_entry ( GObject    *object, GParamSpec *pspec, gpointer    user_data);
void mutter_name_appeared(GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data);
void mutter_name_vanished( GDBusConnection *connection,
        const gchar     *name,
        gpointer         user_data);

void
app_mgr_hk_proxy_clb( GObject *source_object,
                      GAsyncResult *res,
                      gpointer user_data);
void update_selection_popup(MildenhallLauncher *launcher, SearchData *sData,gboolean update);
guint u_find_match_in_model(ThornburyModel *pModel , gchar* pStringToSearch , guint uinColumnNo);

void populate_launcher_from_canterbury_platform (MildenhallLauncher *launcher);

void mildenhall_launcher_activate_by_display_name (MildenhallLauncher *self,
                                                   const gchar *display_name,
                                                   const gchar *launched_by);

#define LAUNCHER_DEBUG

#ifdef LAUNCHER_DEBUG
#define launcher_debug( a ...) \
    {			            	\
        g_print("LAUNCHER:: "a);		\
    }

#else
#define launcher_debug( a ...)

#endif /*LAUNCHER_DEBUG */

#endif
