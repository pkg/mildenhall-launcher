/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "launcher.h"

#include <mildenhall/mildenhall.h>

#include "icon-label-icon-item.h"

guint u_find_match_in_model(ThornburyModel *pModel , gchar* pStringToSearch , guint uinColumnNo)
{
	guint uinRowNo = -1 ;

	if(G_IS_OBJECT(pModel))
	{
		guint uinTotalRows = thornbury_model_get_n_rows(pModel);
		guint uinIter ;
		for(uinIter = 0 ; uinIter < uinTotalRows ; uinIter++)
		{
			ThornburyModelIter *pIter = thornbury_model_get_iter_at_row(pModel, uinIter);
			if(NULL != pIter)
			{
				GValue value = { 0, };
				thornbury_model_iter_get_value(pIter,uinColumnNo, &value);
				gchar *pStringInModel = (gchar *)g_value_get_string(&value);
				if(!g_strcmp0(pStringInModel , pStringToSearch))
				{
					/* Match Found */
					//return TRUE;
					g_value_unset (&value);
					g_object_unref(pIter);
					uinRowNo = uinIter ;
					break;
				}
				g_value_unset (&value);
			}
			g_object_unref(pIter);
		}
	}
	return uinRowNo;
}

static void v_change_model_string_field (ThornburyModelIter *pIter , guint uinColunm , gchar *pText )
{
	GValue pIcon = G_VALUE_INIT;
	g_value_init (&pIcon, G_TYPE_STRING);
	g_value_set_static_string (&pIcon, pText);
	thornbury_model_iter_set_value(pIter  ,uinColunm ,&pIcon );
	g_value_unset (&pIcon);
}

static void v_change_model_float_field (ThornburyModelIter *pIter , guint uinColunm , gdouble pValue )
{
	GValue gValue = G_VALUE_INIT;
	g_value_init (&gValue, G_TYPE_FLOAT);
	g_value_set_float(&gValue, pValue);
	thornbury_model_iter_set_value(pIter  ,uinColunm ,&gValue );
	g_value_unset (&gValue);
}

static void v_change_model_guint_field (ThornburyModelIter *pIter , guint uinColunm , guint pValue )
{
	GValue gValue = G_VALUE_INIT;
	g_value_init (&gValue, G_TYPE_UINT);
	g_value_set_uint(&gValue, pValue);
	thornbury_model_iter_set_value(pIter  ,uinColunm ,&gValue );
	g_value_unset (&gValue);
}

/*
 * Apply K E R N I N G on @name.
 * Add spaces and translate to upper case.
 *
 * Returns: (transfer full): @name, in upper case, with spaces inserted
 */
static gchar *
mangle_name (const gchar *name)
{
  gchar *ret;
  guint i;

  if (name == NULL)
    return NULL;

  ret = g_new0 (gchar, strlen (name) * 2);

  for (i = 0; name[i] != '\0'; i++)
    {
      ret[i * 2] = g_ascii_toupper (name[i]);

      if (name[i + 1] == '\0')
        ret[i * 2 + 1] = '\0';
      else
        ret[i * 2 + 1] = ' ';
    }

  return ret;
}

gchar *
launcher_get_mangled_category_label (CbyEntryPoint *entry_point)
{
  g_autofree gchar *category_label = cby_entry_point_get_string (entry_point,
                                                                 "X-Apertis-CategoryLabel");
  return mangle_name (category_label);
}

gchar *
launcher_get_mangled_display_name (CbyEntryPoint *entry_point)
{
  const gchar *display_name = cby_entry_point_get_display_name (entry_point);
  return mangle_name (display_name);
}

gchar *
launcher_get_category_icon (CbyEntryPoint *entry_point)
{
  /* this gets appended to mildenhall-launcher's ${datadir} to form a filename */
  g_autofree gchar *category_icon = cby_entry_point_get_string (entry_point,
                                                                "X-Apertis-CategoryIcon");
  return g_strdup_printf ("/%s.png", category_icon);
}

gchar *
launcher_get_icon_uri (CbyEntryPoint *entry_point)
{
  gchar *ret = NULL;
  GtkIconTheme *theme = gtk_icon_theme_get_default ();
  GIcon *icon = cby_entry_point_get_icon (entry_point);
  if (icon == NULL)
    return NULL;

  if (G_IS_THEMED_ICON (icon))
    {
      /* FIXME: As Apertis Application Bundle Specification mentioned,
       * a preferred size in Apertis isn't clear. The size, 36, is taken
       * because most of bundles provides that size of icon.
       * https://developer.apertis.org/documentation/bundle-spec.html#icon-for-the-bundle */
      g_autoptr (GtkIconInfo) icon_info = gtk_icon_theme_lookup_by_gicon (theme, icon, 36, 0);
      g_autoptr (GError) error = NULL;

      if (icon_info == NULL)
        return NULL;

      ret = g_filename_to_uri (gtk_icon_info_get_filename (icon_info),
                               NULL,
                               &error);

      if (error != NULL)
        {
          g_warning ("Failed to convert filename to uri (reason: %s)", error->message);
        }
    }
  else if (G_IS_FILE_ICON (icon))
    {
      GFile *file = g_file_icon_get_file (G_FILE_ICON (icon));

      if (file)
        ret = g_file_get_uri (file);
    }
  else
    {
      launcher_debug ("icon of type %s not handled yet", G_OBJECT_TYPE_NAME (icon));
    }

  return ret;
}

static void
entry_point_index_added_cb (CbyEntryPointIndex *entry_point_index,
                            CbyEntryPoint *entry_point,
                            gpointer user_data)
{
  MildenhallLauncher *launcher = MILDENHALL_LAUNCHER (user_data);
  MildenhallLauncherPrivate *priv = launcher->priv;
  g_autofree gchar *main_menu_label = launcher_get_mangled_category_label (entry_point);
  g_autofree gchar *main_menu_icon = launcher_get_category_icon (entry_point);
  g_autofree gchar *quick_menu_label = launcher_get_mangled_display_name (entry_point);
  g_autofree gchar *quick_menu_icon = launcher_get_icon_uri (entry_point);
  guint new_app_status = CANTERBURY_APPSTORE_APP_STATE_INSTALLED;
  gdouble progress = 100;
  gchar *pIconPath = NULL;
  enLaunchstates enAppState = APP_LAUNCH_ENABLED;

  /* FIXME:  Workaround to ignore entry points that don't have a X-Apertis-CategoryLabel set
   * (required to construct menus anyway), so that agents for example are skipped (agents should
   * not have X-Apertis-CategoryLabel set).
   *
   * Ideally we should check the entry point executable type to determine whether it is an app
   * or agent and only create menus for apps, but canterbury doesn't export an API for that atm,
   * so the workaround should do for now.
   */
  if (main_menu_label == NULL)
      return;

  /* FIXME: This doesn't make sense - we just set new_app_status to STATE_INSTALLED above */
  if (CANTERBURY_APPSTORE_LOCAL_APP_STATE_FAILED == new_app_status)
    {
      launcher_debug ("  menu entry removed %s \n", quick_menu_label);
      remove_menu_entry_from_model (launcher, quick_menu_label);
      return;
    }

  guint uinQuikMenuEntry = -1;
  uinQuikMenuEntry = u_find_match_in_model (priv->quick_menu_model, (gchar *) main_menu_label, ILI_COLUMN_NAME);
  if (-1 != uinQuikMenuEntry)
    {
      ThornburyModelIter *pIter = thornbury_model_get_iter_at_row (priv->quick_menu_model, uinQuikMenuEntry);
      if (NULL != pIter)
        {
          v_change_model_string_field (pIter, ILI_COLUMN_NAME, g_strdup ((gchar *) quick_menu_label));
          LAUNCHER_CHECK_IMAGE_PATH (quick_menu_icon, pIconPath);
          v_change_model_string_field (pIter, ILI_COLUMN_ICON, pIconPath);
          v_change_model_string_field (pIter, ILI_COLUMN_LABEL, g_strdup ((gchar *) quick_menu_label));
          v_change_model_string_field (pIter, ILI_COLUMN_ICON2, pIconPath);
          v_change_model_string_field (pIter, ILI_COLUMN_STATUS_TEXT, g_strdup (" "));
          v_change_model_float_field (pIter, ILI_COLUMN_PROGRESS, (gfloat) (progress / 100));
          v_change_model_guint_field (pIter, ILI_COLUMN_LAUNCH_ACTION, enAppState);
          g_object_unref (pIter);
          LAUNCHER_FREE_MEM_IF_NOT_NULL (pIconPath);
        }
    }
  else
    {
      if (is_item_in_model (priv->main_menu_model, main_menu_label))
        {
          insert_quick_menu_entry_into_model (launcher,
                                              priv->quick_menu_model,
                                              main_menu_label,
                                              quick_menu_label,
                                              quick_menu_icon,
                                              " ",
                                              progress);
        }
      else
        {
          append_new_menu_entry_into_model (launcher,
                                            main_menu_label, main_menu_icon,
                                            quick_menu_label, quick_menu_icon,
                                            " ", progress);
        }
    }
}

static void
entry_point_index_removed_cb (CbyEntryPointIndex *entry_point_index,
                              CbyEntryPoint *entry_point,
                              gpointer user_data)
{
  MildenhallLauncher *launcher = MILDENHALL_LAUNCHER (user_data);
  g_autofree gchar *quick_menu_label = launcher_get_mangled_display_name (entry_point);

  launcher_debug ("  menu entry removed %s \n", quick_menu_label);
  remove_menu_entry_from_model (launcher, quick_menu_label);
}

static void
entry_point_index_changed_cb (CbyEntryPointIndex *entry_point_index,
                              CbyEntryPoint *entry_point,
                              gpointer user_data)
{
  /* FIXME: improve it, update instead of removing/adding */
  entry_point_index_removed_cb (entry_point_index, entry_point, user_data);
  entry_point_index_added_cb (entry_point_index, entry_point, user_data);
}

void
populate_launcher_from_canterbury_platform (MildenhallLauncher *launcher)
{
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;
  g_autoptr (GPtrArray) entry_points = NULL;
  g_autoptr (GPtrArray) main_menu_label = g_ptr_array_new_with_free_func (g_free);
  g_autoptr (GPtrArray) main_menu_icon = g_ptr_array_new_with_free_func (g_free);
  g_autoptr (GPtrArray) quick_menu_label = g_ptr_array_new_with_free_func (g_free);
  g_autoptr (GPtrArray) quick_menu_icon = g_ptr_array_new_with_free_func (g_free);
  CbyEntryPoint *entry_point;
  guint i;

  g_signal_connect (priv->entry_point_index,
                    "added",
                    G_CALLBACK (entry_point_index_added_cb),
                    launcher);
  g_signal_connect (priv->entry_point_index,
                    "removed",
                    G_CALLBACK (entry_point_index_removed_cb),
                    launcher);
  g_signal_connect (priv->entry_point_index,
                    "changed",
                    G_CALLBACK (entry_point_index_changed_cb),
                    launcher);

  entry_points = cby_entry_point_index_get_entry_points (priv->entry_point_index);
  for (i = 0; i < entry_points->len; i++)
    {
      entry_point = g_ptr_array_index (entry_points, i);
      if (cby_entry_point_should_show (entry_point))
        {
          g_autofree gchar *mangled_category_label = launcher_get_mangled_category_label (entry_point);
          g_autofree gchar *category_icon = launcher_get_category_icon (entry_point);
          g_autofree gchar *mangled_display_name = launcher_get_mangled_display_name (entry_point);
          g_autofree gchar *icon_uri = launcher_get_icon_uri (entry_point);
          g_ptr_array_add (main_menu_label, g_steal_pointer (&mangled_category_label));
          g_ptr_array_add (main_menu_icon, g_steal_pointer (&category_icon));
          g_ptr_array_add (quick_menu_label, g_steal_pointer (&mangled_display_name));
          g_ptr_array_add (quick_menu_icon, g_steal_pointer (&icon_uri));
        }
    }

  g_ptr_array_add (main_menu_label, NULL);
  g_ptr_array_add (main_menu_icon, NULL);
  g_ptr_array_add (quick_menu_label, NULL);
  g_ptr_array_add (quick_menu_icon, NULL);

  populate_rollers (launcher,
                    (gchar **) main_menu_label->pdata,
                    (gchar **) main_menu_icon->pdata,
                    (gchar **) quick_menu_label->pdata,
                    (gchar **) quick_menu_icon->pdata);
}

void
launcher_proxy_clb (GObject *source_object,
                    GAsyncResult *res,
                    gpointer user_data)
{
  GError *error;

 if(! MILDENHALL_LAUNCHER(user_data))
  return;

  MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
  MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

  /* finishes the proxy creation and gets the proxy ptr */
  priv->launcher_proxy = canterbury_launcher_proxy_new_finish (res, &error);

  if (priv->launcher_proxy == NULL)
    {
      g_printerr ("error %s \n", g_dbus_error_get_remote_error (error));
      return;
    }

  g_signal_connect (priv->launcher_proxy,
                    "notify::current-active-menu-entry",
                    G_CALLBACK (on_notify_current_active_menu_entry),
                    user_data);
}


void
app_mgr_launcher_service_name_appeared ( GDBusConnection *connection,
                                         const gchar     *name,
                                         const gchar     *name_owner,
                                         gpointer         user_data)
{
	launcher_debug("app manager launcher service name_appeared \n");

	/* Asynchronously creates a proxy for the D-Bus interface */
	canterbury_launcher_proxy_new (
			connection,
			G_DBUS_PROXY_FLAGS_NONE,
			"org.apertis.Canterbury",
			"/org/apertis/Canterbury/Launcher",
			NULL,
			launcher_proxy_clb,
			user_data );
}



void
app_mgr_launcher_service_name_vanished( GDBusConnection *connection,
                                        const gchar     *name,
                                        gpointer         user_data)
{

 if(! MILDENHALL_LAUNCHER(user_data))
  return;

 MildenhallLauncher *launcher = MILDENHALL_LAUNCHER(user_data);
 MildenhallLauncherPrivate *priv = MILDENHALL_LAUNCHER (launcher)->priv;

 launcher_debug("app_mgr_name_vanished \n");
 if(NULL != priv->launcher_proxy)
 g_object_unref(priv->launcher_proxy);

}

static void
activate_cb (GObject *source_object,
             GAsyncResult *result,
             gpointer nil G_GNUC_UNUSED)
{
  g_autoptr (GError) error = NULL;
  CbyEntryPoint *entry_point = CBY_ENTRY_POINT (source_object);

  if (cby_entry_point_activate_finish (entry_point, result, &error))
    {
      g_debug ("Successfully activated %s (%s)",
               cby_entry_point_get_id (entry_point),
               cby_entry_point_get_display_name (entry_point));
    }
  else
    {
      g_warning ("Failed to activate %s (%s): %s",
                 cby_entry_point_get_id (entry_point),
                 cby_entry_point_get_display_name (entry_point),
                 error->message);
    }
}

void
mildenhall_launcher_activate_by_display_name (MildenhallLauncher *self,
                                              const gchar *display_name,
                                              const gchar *launched_by)
{
  MildenhallLauncherPrivate *priv = self->priv;
  g_autoptr (GPtrArray) entry_points = NULL;
  CbyEntryPoint *entry_point;
  guint i;

  entry_points = cby_entry_point_index_get_entry_points (priv->entry_point_index);

  for (i = 0; i < entry_points->len; i++)
    {
      g_autofree gchar *mangled = NULL;

      entry_point = g_ptr_array_index (entry_points, i);

      /* This is of course horribly inefficient, but I'm not sure whether
       * it's safe to add a non-displayed column to the roller model to
       * store the entry points. */
      mangled = launcher_get_mangled_display_name (entry_point);

      if (cby_entry_point_should_show (entry_point) &&
          g_strcmp0 (display_name, mangled) == 0)
        {
          GVariantDict vardict;

          g_variant_dict_init (&vardict, NULL);

          if (launched_by != NULL)
            g_variant_dict_insert (&vardict, "X-Apertis-LaunchedBy",
                                   "s", launched_by);

          cby_entry_point_activate_async (entry_point,
                                          g_variant_dict_end (&vardict),
                                          NULL, activate_cb, NULL);
          return;
        }
    }
}
